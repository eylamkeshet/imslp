class ScoreController < ApplicationController
  #encoding: UTF-8
  require 'open-uri'
  require 'nokogiri'
  require 'mechanize'
  require 'json'
  require 'openssl'
  OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

  def index
    # Array with IMSLP results and subresults* (*Every link inside the result)
    @imslp_results = []
    # Agent for opening web pages
    agent = Mechanize.new
    # Search param
    params.has_key?(:search)? str = params[:search] : str = ""
    # Open Google query with the param from IMSLP
    google_results = agent.get(URI.encode("http://www.google.com/search?num=5&q=site:imslp.org " + str))

    google_results.links_with(:href => %r{\?q=http:\/\/imslp.org\/wiki} ).each do |link|
     # Format URL from Google results
     formatted_link = /(?<=url\?q=)(.*)(?=&sa)/.match(URI.decode(link.href))
     # Hash of IMSLP subresults
     imslp_subresults = []
     # Open the IMSLP page from the google results
     imslp_page = agent.get(formatted_link)
     # Get the div containing the scores
     imslp_page.search('//div[starts-with(@id, "tab")]').each do |e|
       if(!e.text.strip.include?("Recordings") && !e.text.strip.include?("MIDI"))
         tab_links = []
         # Fetch every "we" class from tabs - get the edition info and organize the uls
         e.css(".we").each do |elem|
           edition_links = []
           edition_info = []
           elem.css(".we_edition_entry").map { |i| edition_info << [i.previous_element.text.gsub(":",""), i.text] }
           # Remove last element of edition info - purchase with js links
           edition_info.pop
           # Regex that matches the download links
           re = /Special:ImagefromIndex/
           # Insert the links
           edition_links << [edition_info, elem.css('a').select{ |n| n['href'] =~ re }]
           tab_links << edition_links
         end
         imslp_subresults << tab_links
       end
     end

     # Add to results only if scores are contained
     if(imslp_subresults.any?)
       # The return array contains the a substring of the title and an array of links
       @imslp_results << [imslp_page.title.gsub(" - IMSLP/Petrucci Music Library: Free Public Domain Sheet Music",""),imslp_subresults]
     end

    end
  end

  def get_pdf
    agent = Mechanize.new
    url = params[:pdf_url]
    # Getting the page while sending a cookie that accept the trems
    url.sub! 'ImagefromIndex', 'IMSLPDisclaimerAccept'
    page = agent.post(url, {'imslpdisclaimeraccepted' => 'yes'})
    if(!!(page.uri.to_s =~ /\/linkhandler\.php\?path=/))
      #EU or CA links handling
      eu_url = (page.uri.to_s).gsub("linkhandler.php?path=","files")
      redirect_to eu_url
    else
      #Check if the link is already PDF or needs to go one level deeper
      parsed_json_headers = JSON.parse(page.response.to_json)
      if (parsed_json_headers['content-type'] != "application/pdf")
        redirect_pdf = page.at("#sm_dl_wait").attributes["data-id"].to_s
      else
        redirect_pdf = page.uri.to_s
      end
      redirect_to redirect_pdf
    end
  end


end
